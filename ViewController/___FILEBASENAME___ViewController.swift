//___FILEHEADER___

import UIKit

class ___VARIABLE_productName___ViewController: UIViewController {
    
    // MARK: - Properties
    
    var presenter: ___VARIABLE_productName___PresenterProtocol?
    
    // MARK: - UIViewController Events
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
