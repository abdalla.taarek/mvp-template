// MARK: - ___VARIABLE_productName___Presenter Delegate

extension ___VARIABLE_productName___ViewController: ___VARIABLE_productName___ViewProtocol {
    
    func showIndicator() {
        
    }
    
    func hideIndicator() {
        
    }
    
    func fetchingDataSuccess() {
        
    }
    
    func showError(error:String) {
        
    }
    
}
